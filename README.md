# SPM - Soul Package Manager

## Confugre PATH to use application

- Clone repo.
- Set exexute rules to the spm file
```
chmod +x spm/bin/spm
```

- Update .bashrc config file
```
vim ~/.bashrc
```

- Then add to the last file lines

```
export SPM_HOME=$HOME/projects/self/soulmirror/spm
export PATH=$PATH:$SPM_HOME/bin
```

- Then we should reload resources

```
source ~/.bashrc
```

